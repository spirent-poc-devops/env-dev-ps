#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [string] $ConfigPath,

    [Parameter(Mandatory = $true, Position = 1)]
    [string] $ResourcePath
)

# Read config and resources
$config = Read-EnvConfig -ConfigPath $ConfigPath
$resources = Read-EnvResources -ResourcePath $ResourcePath

$namespace = Get-EnvMapValue -Map $config -Key "k8s.namespace"
$apisixEtcdReplicaCount = Get-EnvMapValue -Map $config -Key "apisix.etcd_replica_count"
$adminToken = -join ((48..57)+(97..122) |Get-Random -Count 32 |%{[char]$_})
$viewerToken = -join ((48..57)+(97..122) |Get-Random -Count 32 |%{[char]$_})

# Todo: replace helm chart with a yml file to use private images
helm repo add apisix https://charts.apiseven.com
helm repo update
helm install apisix apisix/apisix `
  --set gateway.type=NodePort `
  --set ingress-controller.enabled=true `
  --set ingress-controller.config.apisix.serviceNamespace=$namespace `
  --set admin.type=NodePort `
  --set admin.credentials.admin=$adminToken `
  --set admin.credentials.viewer=$viewerToken `
  --set etcd.replicaCount=$apisixEtcdReplicaCount `
  --namespace $namespace

Set-EnvMapValue -Map $resources -Key "apisix.admin_token" -Value $adminToken
Set-EnvMapValue -Map $resources -Key "apisix.viewer_token" -Value $viewerToken
Write-EnvResources -ResourcePath $ResourcePath -Resources $resources

# Wait apisix to start
Write-Host "Waiting for apisix controller to run...`nUsually it takes about 5 mins."
do {
    Start-Sleep -Seconds 10
    $res = $(kubectl get pods -n $namespace -l app.kubernetes.io/instance=apisix --field-selector=status.phase!=Running)
} while ($res -ne $null);

# Record results and save them to disk
$port = $(kubectl get --namespace $namespace -o jsonpath="{.spec.ports[0].nodePort}" services apisix-gateway)
$adminPort = $(kubectl get --namespace $namespace -o jsonpath="{.spec.ports[0].nodePort}" services apisix-admin)
# $serviceHost = $(kubectl get nodes --namespace $namespace  -o jsonpath="{.items[0].status.addresses[0].address}")
$serviceHost = "apisix-gateway." + $namespace + ".svc.cluster.local"
$endpoint = "$($serviceHost):$($port)"

Set-EnvMapValue -Map $resources -Key "apisix.state" -Value "deployed"
Set-EnvMapValue -Map $resources -Key "apisix.port" -Value $port
Set-EnvMapValue -Map $resources -Key "apisix.admin_port" -Value $adminPort
Set-EnvMapValue -Map $resources -Key "apisix.host" -Value $serviceHost
Set-EnvMapValue -Map $resources -Key "apisix.endpoint" -Value $endpoint
Write-EnvResources -ResourcePath $ResourcePath -Resources $resources
