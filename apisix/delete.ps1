#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [string] $ConfigPath,

    [Parameter(Mandatory = $true, Position = 1)]
    [string] $ResourcePath
)

# Read config and resources
$config = Read-EnvConfig -ConfigPath $ConfigPath
$resources = Read-EnvResources -ResourcePath $ResourcePath

$namespace = Get-EnvMapValue -Map $config -Key "k8s.namespace"

# Delete component
helm uninstall apisix -n $namespace

# Delete pvc?
# kubectl delete pvc -n $namespace -l app.kubernetes.io/instance=apisix --ignore-not-found=true

# Delete results and save resource file to disk
if (Test-EnvMapValue -Map $resources -Key "apisix") {
    Remove-EnvMapValue -Map $resources -Key "apisix.state"
    Remove-EnvMapValue -Map $resources -Key "apisix.port"
    Remove-EnvMapValue -Map $resources -Key "apisix.admin_port"
    Remove-EnvMapValue -Map $resources -Key "apisix.host"
    Remove-EnvMapValue -Map $resources -Key "apisix.endpoint"
    Remove-EnvMapValue -Map $resources -Key "apisix.viewer_token"
    Remove-EnvMapValue -Map $resources -Key "apisix.admin_token"
    Write-EnvResources -ResourcePath $ResourcePath -Resources $resources
}
