#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [string] $ConfigPath,

    [Parameter(Mandatory = $true, Position = 1)]
    [string] $ResourcePath
)

# Read config and resources
$config = Read-EnvConfig -ConfigPath $ConfigPath
$resources = Read-EnvResources -ResourcePath $ResourcePath

# Generate template
. "$PSScriptRoot/templates.ps1" -Config $config -Resources $resources

# Create kubernetes objects
kubectl apply -f "$PSScriptRoot/../temp/connections.yml"

# Record results and save them to disk
Set-EnvMapValue -Map $resources -Key "connections.status" -Value "created"
Write-EnvResources -ResourcePath $ResourcePath -Resources $resources
