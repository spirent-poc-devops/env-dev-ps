#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [hashtable] $Config,

    [Parameter(Mandatory = $true, Position = 1)]
    [hashtable] $Resources
)

# Read parameters
$dockerAuth = Get-EnvMapValue -Map $config -Key "docker.auth"
$dockerRegistry = Get-EnvMapValue -Map $config -Key "docker.registry"
$dockerUsername = Get-EnvMapValue -Map $config -Key "docker.username"
$dockerPassword = Get-EnvMapValue -Map $config -Key "docker.password"
$dockerEmail = Get-EnvMapValue -Map $config -Key "docker.email"
$namespace = Get-EnvMapValue -Map $config -Key "k8s.namespace"
$k8s_address = Get-EnvMapValue -Map $resources -Key "k8s.address"
$redis_host = Get-EnvMapValue -Map $resources -Key "redis.host"
$redis_port = Get-EnvMapValue -Map $resources -Key "redis.port"
$kafka_host = Get-EnvMapValue -Map $resources -Key "kafka.host"
$kafka_port = Get-EnvMapValue -Map $resources -Key "kafka.port"
$neo4j_host = Get-EnvMapValue -Map $resources -Key "neo4j.host"
$neo4j_port = Get-EnvMapValue -Map $resources -Key "neo4j.port"
$neo4j_bolt_port = Get-EnvMapValue -Map $resources -Key "neo4j.bolt_port"
$neo4j_name = Get-EnvMapValue -Map $config -Key "neo4j.name"
$neo4j_username = Get-EnvMapValue -Map $config -Key "neo4j.username"
$neo4j_password = Get-EnvMapValue -Map $config -Key "neo4j.password"
$timescale_host = Get-EnvMapValue -Map $resources -Key "timescale.host"
$timescale_port = Get-EnvMapValue -Map $resources -Key "timescale.port"
$timescale_name = Get-EnvMapValue -Map $config -Key "timescale.name"
$timescale_username = Get-EnvMapValue -Map $config -Key "timescale.username"
$timescale_password = Get-EnvMapValue -Map $config -Key "timescale.password"
$logging_host = Get-EnvMapValue -Map $resources -Key "logging.host"
$logging_port = Get-EnvMapValue -Map $resources -Key "logging.port"
$metrics_host = Get-EnvMapValue -Map $resources -Key "metrics.host"
$metrics_port = Get-EnvMapValue -Map $resources -Key "metrics.port"

# Add extra connection parameters
$temp = Get-EnvMapValue -Map $config -Key "connection_params"
if ($null -eq $temp) { $temp = @{} }
if ($null -eq $temp["external_api_host"]) { $temp["external_api_host"] = $k8s_address }
$extra_params = "";
foreach ($item in $temp.GetEnumerator()) {
    $extra_params = $extra_params + "  " + $item.Name + ": `"" + $item.Value + "`"`n"
}

# Add extra connection credentials
$temp = Get-EnvMapValue -Map $config -Key "connection_creds"
if ($null -eq $temp) { $temp = @{} }
$extra_creds = "";
foreach ($item in $temp.GetEnumerator()) {
    $extra_creds = $extra_creds + "  " + $item.Name + ": `"" + $item.Value + "`"`n"
}

# Set template parameters
$templateParams = @{ 
    docker_auth        = $dockerAuth;
    docker_registry    = $dockerRegistry;
    docker_username    = $dockerUsername;
    docker_password    = $dockerPassword;
    docker_email       = $dockerEmail;
    namespace          = $namespace; 
    k8s_address        = $k8s_address;
    redis_host         = $redis_host;
    redis_port         = $redis_port;
    kafka_host         = $kafka_host;
    kafka_port         = $kafka_port;
    neo4j_host         = $neo4j_host;
    neo4j_port         = $neo4j_port;
    neo4j_bolt_port    = $neo4j_bolt_port;
    neo4j_name         = $neo4j_name;
    neo4j_username     = $neo4j_username;
    neo4j_password     = $neo4j_password;
    timescale_host     = $timescale_host;
    timescale_port     = $timescale_port;
    timescale_name     = $timescale_name;
    timescale_username = $timescale_username;
    timescale_password = $timescale_password;
    logging_host       = $logging_host;
    logging_port       = $logging_port;
    metrics_host       = $metrics_host;
    metrics_port       = $metrics_port;
    extra_params       = $extra_params;
    extra_creds        = $extra_creds;
}

# Generate template
Build-EnvTemplate -InputPath "$PSScriptRoot/templates/connections.yml" -OutputPath "$PSScriptRoot/../temp/connections.yml" -Params1 $templateParams
