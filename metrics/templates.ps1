#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [hashtable] $Config,

    [Parameter(Mandatory = $true, Position = 1)]
    [hashtable] $Resources
)

# Read parameters
$namespace = Get-EnvMapValue -Map $config -Key "k8s.namespace"

# Set template parameters
$templateParams = @{
    namespace = $namespace;
}

# Generate template
Build-EnvTemplate -InputPath "$PSScriptRoot/templates/metrics.yml" -OutputPath "$PSScriptRoot/../temp/metrics.yml" -Params1 $templateParams
