param
(
    [Alias("c", "Config")]
    [Parameter(Mandatory=$false, Position=0)]
    [string] $ConfigPath = "./config/default_config.json"
)

# Load support functions
. "./common/include.ps1"

# --- Install Chocolatey ---
# The commands below for Chocolatey install must be executed from an elevated PS session. 
# (Run "Start-Process PowerShell -Verb RunAs" to open a new Powershell process as Administrator)

# If Get-ExecutionPolicy is "Restricted", we must bypass it during the installation process
# (Scope of the bypass is just this process)
Set-ExecutionPolicy Bypass -Scope Process -Force 
[System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072 
iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))

# --- Install docker and kubernetes ---
choco install --yes kubernetes-cli 
choco install --yes docker
choco install --yes virtualbox
choco install --yes minikube --version 1.17.0
choco install --yes kubernetes-helm
choco install --yes openssh