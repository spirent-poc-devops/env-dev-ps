# <img src="https://upload.wikimedia.org/wikipedia/en/thumb/c/ce/Spirent_logo.svg/490px-Spirent_logo.svg.png" alt="Spirent Logo" width="200"> Spirent VisionWorks 2.0 Development Environment

This is the environment automation scripts to create, update and delete Development Environments. The Development environments are functionally 
identical to production environments, but able to fit and run on a regular development computer. 

The environment consists of the following components:
* 1-node **Kubernetes** cluster running as minikube (docker desktop)
* **Timescale** database running in Kubernete (operational and historical storage)
* **Neo4j** database running in Kubernetes (storage for graph data)
* **Kafka** message broker running in Kubernetes
* **Redis** distributed cache running in Kubernetes 

<img src="design.png" width="800">

<a name="links"></a> Quick links:

* [Max Infrastructure Requirements](https://docs.google.com/document/d/1T9QJ9RzZETci0bB9x1uksFim43Pm3Uue6JiBRrZUPvw/edit)
* [Max Infrastructure Architecture](https://docs.google.com/document/d/1rPub_d4HwQ2P87GCc2vw5wf8KB7GOKUDvb9T9JYy4MI/edit)
* [Change Log](CHANGELOG.md)


# Project structure
| Folder | Description |
|----|----|
| common | Scripts with support functions like working with configs, templates etc. | 
| config | Config files for scripts. Also stores *resources* files, created automaticaly. | 
| kafka | Contains scripts for provisioning the kafka component. |
| kubernetes | Contains scripts for provisioning the kubernetes component. |
| redis | Contains scripts for provisioning the redis component. |
| temp | Folder for storing automaticaly created temporary files (e.g. populated templates). | 
| timescale | Contains scripts for provisioning the timescale database. |
| neo4j | Contains scripts for provisioning the neo4j database. |


## Use

Start the process from any computer. Download released package with environment provisioning scripts from [link...](http://somewhere.com/env-dev-ps-1.0.0.zip)
```bash
wget http://somewhere.com/env-dev-ps-1.0.0.zip
```

Unzip scripts from the package
```bash
unzip env-dev-ps-1.0.0.zip
```

Go to the `config` folder and create configuration for a new environment.
Read configuration section below and use [sample_config.json](config/sample_config.json)
as your starting point.

Your configuration may look like the sample below.
```json
{
  "environment": {
    "type": "dev",
    "version": "1.0.0"
  },
  "docker": {
    "username": "XXXXX",
    "email": "XXXX.XXXX@spirent.com",
    "password": "XXXX",
    "registry": "ghcr.io"
  },
  "k8s": {
    "hyperv_switch": "",
    "minikube_home": "",
    "blobs_storage_gb": 5,
    "memory": 4096,
    "version": "v1.22.0",
    "cpus": 2,
    "namespace": "infra",
    "driver": "docker"
  },
  "redis": {
    "password": "admin",
    "replicas_count": 1
  },
  "kafka": {
    "version": "12.17.6"
  },
  "neo4j": {
    "username": "neo4j",
    "name": "spirentlocal",
    "password": "SPIRENTadmin2021#",
    "version": "4.3"
  },
  "timescale": {
    "username": "spirentadmin",
    "version": "12",
    "name": "spirentlocal",
    "password": "SPIRENTadmin2021#"
  },
  "connection_params": {
    "external_api_protocol": "https",
    "external_api_host": "api.visionworks.spirent.com",
    "external_api_port": "80"
  },
  "connection_creds": {
  }
}
```

Install prerequisites on your computer by running the installation script for your platform.
```bash
install_prereq_[win | mac | ubuntu].[ps1 | sh]
```

If the machine has Hyper-V enabled: 
* Change the *k8s.driver* config parameter to "hyperv";
* Make sure to [create an external switch](https://octopus.com/blog/minikube-on-windows#:~:text=Minikube%20requires%20an%20external%20HyperV,The%20HyperV%20actions%20menu.), if one doesn't already exist;
* Add the name of the external switch to the *k8s.hyperv_switch* config parameter.

Otherwise, leave the *k8s.driver* config parameter equal to "virtualbox" or "docker". docker works for macOS

Create the environment:
```bash
create_env.ps1 -Config <path to config file>
```

After you run the script you shall see a resource file (ending with `resources.json`) next to the config file you created.

Delete the environment when its no longer needed to free up used computing resources.
```bash
delete_env.ps1 -Config <path to config file>
```

## Accessing apps

A loadbalancer is installed within minikube to access services inside the cluster based on ingress rules deployed. To expose the loadbalancer externally you need to run the following command:

```bash
minikube tunnel
```

Loadbalancer would be exposed at `127.0.0.1`. More details [here](https://minikube.sigs.k8s.io/docs/handbook/accessing/#loadbalancer-access)

## Configuration

The environment configuration supports the following configuration parameters:

| Parameter | Default value | Description |
|----|----|---|
| type | dev | Type of environment |
| minikube_home |  | Path of *.minikube* folder, if it is not located in home directory.  |
| k8s.version | v1.9.4 | Kuberntes cluster version |
| k8s.driver | "virtualbox" or "hyperv" or "docker" | Driver to use for the minikube kubernetes cluster |
| k8s.hyperv_switch | "External Switch" | External Hyper-V Switch to use for the minikube kubernetes cluster |
| k8s.memory | 8198 | Memory allocated to minikube vm |
| k8s.cpus | 2 | Minikube cpu count |
| redis.replicas_count | 1 | Number of Redis replicas to provision |
| redis.password | "admin" | Redis' password |
| neo4j.version | "4.3" | Timescale version (must be 11 or higher to use with Docker) |
| neo4j.username | "spirentadmin" | Username for the neo4j DB |
| neo4j.password | "123" | Password for the neo4j DB |
| timescale.version | "12" | Timescale version (must be 11 or higher to use with Docker) |
| timescale.name | "spirentlocalpg" | Name for the timescale DB |
| timescale.username | "timescaleadmin" | Username for the timescale DB |
| timescale.password | "admin123" | Password for the timescale DB |


## Resources

As the result of the scripts information about created resources will be saved into a resource file that ends with `resources.json`
and placed next to the configuration file used to create an environment.

Resource parameters list:
| Parameter | Ex. value | Description |
|----|----|---|
|k8s.type| minikube | Type of k8s deployment |
|k8s.address| 192.168.99.100 | IP address of the k8s node |
|k8s.ssh_key| C:\\Users\\DevUser\\.minikube\\machines\\minikube\\id_rsa | Location of k8s' ssh key |
|redis.port| 30379 | Redis service's port |
|kafka.port| 30092 | Kafka service's port |
|timescale.port| 30877 | Timescale DB's port |
|neo4j.port| 30880 | Neo4j DB's port |
|neo4j.bolt_port| 30881 | Neo4j DB's bolt port |
|consul.state | Deployed | Whether or not consul was successfully deployed |
|connections.state | created | Whether or not the connections config map and connection-creds secret were successfully created | 

Example environment resources file:
```json
{
  "neo4j": {
    "port": "7474",
    "bolt_port": "7687",
    "host": "neo4j.infra.svc.cluster.local",
    "endpoint": "neo4j.infra.svc.cluster.local:7474"
  },
  "logging": {
    "endpoint": "kibana-logging.infra.svc.cluster.local:5601",
    "host": "kibana-logging.infra.svc.cluster.local",
    "port": "5601"
  },
  "kafka": {
    "state": "deployed",
    "port": "9092",
    "host": "kafka.infra.svc.cluster.local",
    "endpoint": "kafka.infra.svc.cluster.local:9092"
  },
  "k8s": {
    "blobs_storage_gb": 5,
    "version": "v1.22.0",
    "type": "minikube",
    "cpus": 2,
    "address": "192.168.49.2",
    "ssh_key": "C:\\Users\\XXX\\.minikube\\machines\\minikube\\id_rsa",
    "namespace": "infra",
    "driver": "docker",
    "memory": 4096
  },
  "metrics": {
    "port": "3000",
    "host": "grafana.infra.svc.cluster.local",
    "endpoint": "grafana.infra.svc.cluster.local:3000"
  },
  "connections": {
    "status": "created"
  },
  "redis": {
    "endpoint": "redis.infra.svc.cluster.local:6379",
    "host": "redis.infra.svc.cluster.local",
    "port": "6379"
  },
  "environment": {
    "create_time": "2021-09-01T12:37:51.4650127-07:00",
    "delete_time": "2021-09-01T07:27:42.0670345-07:00",
    "version": "1.0.0",
    "type": "dev"
  },
  "timescale": {
    "endpoint": "timescale.infra.svc.cluster.local:5432",
    "host": "timescale.infra.svc.cluster.local",
    "port": "5432"
  }
}
```

## Known issues

* Setting low cpu and memory values in the configuration file can cause problems with pods starting up. For example, 2 cpu and 2 GB memory caused problems with the Kakfa pod during testing. Best results are achieved with 4 cpu and 6+ GB memory. 
* Using virtualbox driver for minikube on mac requires some manual intervention: [troubleshooting](https://stackoverflow.com/questions/52277019/how-to-fix-vm-issue-with-minikube-start)
* Install prerequisite script for mac needs to have different brew commands based on the brew version. The script is written assuming brew version 3 and above: [troubleshooting](https://stackoverflow.com/questions/30413621/homebrew-cask-option-not-recognized)
* Scripts require helm v3 which is installed using the prerequisite script. If you have helm v2 already installed then you need to switch to helm v3 for the scripts to work: [details](https://medium.com/faun/easily-switch-between-helm2-and-helm3-using-bash-function-64d2740b5091)
* On Windows 10, when using Docker driver, ports <1024 don't work with the default latest version of OpenSSH (OpenSSH_for_Windows_7.7p1, LibreSSL 2.6.5): [details](https://minikube.sigs.k8s.io/docs/handbook/accessing/#access-to-ports-1024-on-windows-requires-root-permission).
To fix it, OpenSSH must be uninstalled from Settings > Apps > Optional Features, and then installed using chocolatey (this command is included in the install_prereq_win.ps1 script). Opening a new powershell window and running `ssh -V` should return the new 8.0 version. After this, minikube tunnel will work successfully.

## Contacts

This environment was created and currently maintained by the team managed by *Chris Cotton* .

