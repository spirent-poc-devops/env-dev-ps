function Write-EnvError {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $true, Position = 0)]
        [string] $Message,

        [Parameter(Mandatory = $true)]
        [string] $Component,

        [Parameter(Mandatory = $false)]
        [switch] $Delimiter = $false,      

        [Parameter(Mandatory = $false)]
        [string] $Color = "Red"
    )

    Write-EnvLog -Level "Error" -Component $Component -Message $Message -Delimiter $Delimiter -Color $Color
}

function Write-EnvInfo {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $true, Position = 0)]
        [string] $Message,

        [Parameter(Mandatory = $true)]
        [string] $Component,

        [Parameter(Mandatory = $false)]
        [switch] $Delimiter = $false,      

        [Parameter(Mandatory = $false)]
        [string] $Color = "White"
    )

    Write-EnvLog -Level "Info" -Component $Component -Message $Message -Delimiter $Delimiter -Color $Color
}

function Write-EnvWarn {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $true, Position = 0)]
        [string] $Message,

        [Parameter(Mandatory = $true)]
        [string] $Component,

        [Parameter(Mandatory = $false)]
        [switch] $Delimiter = $false,      

        [Parameter(Mandatory = $false)]
        [string] $Color = "White"
    )

    Write-EnvLog -Level "Warn" -Component $Component -Message $Message -Delimiter $Delimiter -Color $Color
}

function Write-EnvDebug {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $true, Position = 0)]
        [string] $Message,

        [Parameter(Mandatory = $true)]
        [string] $Component,

        [Parameter(Mandatory = $false)]
        [switch] $Delimiter = $false,      

        [Parameter(Mandatory = $false)]
        [string] $Color = "White"
    )

    Write-EnvLog -Level "Debug" -Component $Component -Message $Message -Delimiter $Delimiter -Color $Color
}

function Write-EnvLog {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $true, Position = 0)]
        [string] $Message,

        [Parameter(Mandatory = $true)]
        [ValidateSet("Warn", "Error", "Info", "Debug")]
        [string] $Level,

        [Parameter(Mandatory = $true)]
        [string] $Component,

        [Parameter(Mandatory = $false)]
        [bool] $Delimiter = $false,      

        [Parameter(Mandatory = $false)]
        [string] $Color = "White"
    )

    # Determine the log file name
    if ($null -eq $env:ENV_LOG_FILE) {
        $logTime = (Get-Date).toString("yyyy-MM-dd_hh-mm-ss")
        $fileName = "env" + "_" + $logTime + ".log"
        $env:ENV_LOG_FILE = $fileName
    }
    else {
        $fileName = $env:ENV_LOG_FILE
    }

    # Calculate the file path
    if (($null -eq $fileName) -or ($fileName -eq "")) {
        $path = "$PWD/logs/dev_env.log"
    }
    else {
        $path = "$PWD/logs/$fileName"
    }

    # Create the file if it doesn't exist
    if (!(Test-Path $path)) {
        New-Item -Path $path -ItemType File
    }    

    # If resources do not exist yet return an empty result
    if (($null -eq $Level) -or ($Message -eq "")) {
        Write-Error "Missing the log level or message"
    }
    
    # Append message to the log
    $timeStamp = (Get-Date).toString("yyyy/MM/dd HH:mm:ss")
    $Line = "{`"component`":`"$Component`",`"time`":`"$timeStamp`",`"level`":`"$Level`",`"message`":`"$Message`"}"
    Add-Content $path -value $Line   

    # Define a delimiter line
    if ($Delimiter) {
        $Message = "`n***** $Message *****`n"
    }

    # Write the message to console
    if ($Level -eq "Error") {
        Write-Error $Message
    }
    else {
        Write-Host $Message -ForegroundColor $Color
    }
}
