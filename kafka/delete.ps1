#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [string] $ConfigPath,

    [Parameter(Mandatory = $true, Position = 1)]
    [string] $ResourcePath
)

# Read config and resources
$config = Read-EnvConfig -ConfigPath $ConfigPath
$resources = Read-EnvResources -ResourcePath $ResourcePath

$namespace = Get-EnvMapValue -Map $config -Key "k8s.namespace"

# Delete component
helm uninstall kafka -n $namespace
# if ($LastExitCode -ne 0) {
#     Write-EnvError -Component "kafka" "There were errors deleting kafka, Watch logs above" 
# }

# Delete results and save resource file to disk
if (Test-EnvMapValue -Map $resources -Key "kafka") {
    Remove-EnvMapValue -Map $resources -Key "kafka.state"
    Remove-EnvMapValue -Map $resources -Key "kafka.port"
    Remove-EnvMapValue -Map $resources -Key "kafka.host"
    Remove-EnvMapValue -Map $resources -Key "kafka.endpoint"
    Write-EnvResources -ResourcePath $ResourcePath -Resources $resources
}
