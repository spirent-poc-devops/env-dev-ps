#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [string] $ConfigPath,

    [Parameter(Mandatory = $true, Position = 1)]
    [string] $ResourcePath
)

# Read config and resources
$config = Read-EnvConfig -ConfigPath $ConfigPath
$resources = Read-EnvResources -ResourcePath $ResourcePath

$namespace = Get-EnvMapValue -Map $config -Key "k8s.namespace"
$version = Get-EnvMapValue -Map $config -Key "kafka.version"

# Todo: replace helm chart with a yml file to use private images
helm repo add bitnami https://charts.bitnami.com/bitnami 
$null = helm repo update      
# Install the latest version of the Kafka Helm chart
$out = helm upgrade --install kafka bitnami/kafka --version $version --namespace $namespace | Out-String
# Verify install
if (!($out.Contains("STATUS: deployed"))) {
    Write-EnvError -Component "kafka" "Error while deploying kafka component. See logs above."
}
Write-Host $(helm list --filter 'kafka' --namespace $namespace)

# Record results and save them to disk
$port = kubectl get svc kafka -n $namespace -o=jsonpath="{.spec.ports[0].port}"
$serviceHost = "kafka." + $namespace + ".svc.cluster.local"
$endpoint = "$($serviceHost):$($port)"

Set-EnvMapValue -Map $resources -Key "kafka.state" -Value "deployed"
Set-EnvMapValue -Map $resources -Key "kafka.port" -Value $port
Set-EnvMapValue -Map $resources -Key "kafka.host" -Value $serviceHost
Set-EnvMapValue -Map $resources -Key "kafka.endpoint" -Value $endpoint
Write-EnvResources -ResourcePath $ResourcePath -Resources $resources
