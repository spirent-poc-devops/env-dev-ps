#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [string] $ConfigPath,

    [Parameter(Mandatory = $true, Position = 1)]
    [string] $ResourcePath
)

# Read config and resources
$config = Read-EnvConfig -ConfigPath $ConfigPath
$resources = Read-EnvResources -ResourcePath $ResourcePath

if (
    (Get-EnvMapValue -Map $resources -Key "k8s.version") -ne (Get-EnvMapValue -Map $config -Key "k8s.version") -or
    (Get-EnvMapValue -Map $resources -Key "k8s.driver") -ne (Get-EnvMapValue -Map $config -Key "k8s.driver") -or
    (Get-EnvMapValue -Map $resources -Key "k8s.memory") -ne (Get-EnvMapValue -Map $config -Key "k8s.memory") -or
    (Get-EnvMapValue -Map $resources -Key "k8s.cpus") -ne (Get-EnvMapValue -Map $config -Key "k8s.cpus") -or
    (Get-EnvMapValue -Map $resources -Key "k8s.namespace") -ne (Get-EnvMapValue -Map $config -Key "k8s.namespace") -or
    (Get-EnvMapValue -Map $resources -Key "k8s.blobs_storage_gb") -ne (Get-EnvMapValue -Map $config -Key "k8s.blobs_storage_gb")
) {
    Write-Error "Kubernetes configuration changed, to update the environment you need to entirely delete it (delete_env.ps1) and recreate it (create_env.ps1)."
} else {
    Write-Host "Kibernetes configuration didn't change. Skipping..."
}
