#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [string] $ConfigPath,

    [Parameter(Mandatory = $true, Position = 1)]
    [string] $ResourcePath
)

# Read config and resources
$config = Read-EnvConfig -ConfigPath $ConfigPath
$resources = Read-EnvResources -ResourcePath $ResourcePath

# Destroy minikube
minikube delete

if ($LastExitCode -ne 0) {
    Write-EnvError -Component "kubernetes" "There were errors deleting minikube, Watch logs above"
    exit 0
}

# Delete results and save resource file to disk
if (Test-EnvMapValue -Map $resources -Key "k8s") {
    Remove-EnvMapValue -Map $resources -Key "k8s.type"
    Remove-EnvMapValue -Map $resources -Key "k8s.address"
    Remove-EnvMapValue -Map $resources -Key "k8s.ssh_key"
    Remove-EnvMapValue -Map $resources -Key "k8s.memory"
    Remove-EnvMapValue -Map $resources -Key "k8s.version"
    Remove-EnvMapValue -Map $resources -Key "k8s.cpus"
    Remove-EnvMapValue -Map $resources -Key "k8s.blobs_storage_gb"
    Remove-EnvMapValue -Map $resources -Key "k8s.namespace"
    Write-EnvResources -ResourcePath $ResourcePath -Resources $resources
}
