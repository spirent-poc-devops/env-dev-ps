#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [string] $ConfigPath,

    [Parameter(Mandatory = $true, Position = 1)]
    [string] $ResourcePath
)

# Read config and resources
$config = Read-EnvConfig -ConfigPath $ConfigPath
$resources = Read-EnvResources -ResourcePath $ResourcePath

# Verify if k8s cluster was created
if (!((Test-EnvMapValue -Map $resources -Key "k8s") -and (Test-EnvMapValue -Map $resources -Key "k8s.type"))) {
    $cpus = Get-EnvMapValue -Map $config -Key "k8s.cpus"
    $memory = Get-EnvMapValue -Map $config -Key "k8s.memory"
    $version = Get-EnvMapValue -Map $config -Key "k8s.version"
    $minikube_home = Get-EnvMapValue -Map $config -Key "k8s.minikube_home"
    $namespace = Get-EnvMapValue -Map $config -Key "k8s.namespace"
    $blobs_storage_gb = Get-EnvMapValue -Map $config -Key "k8s.blobs_storage_gb"

    # If using the docker driver, check that docker is running
    Write-Host "Checking docker status:"
    $output = docker info | Out-String
    if ($output.Contains("error during connect")) {
        Write-Host "Received `"error during connect`" - attempting to start docker..."
        # try to start docker
        if ($IsMacOS) {
            # macos
            if (!(Test-Path "/Applications/Docker.app")) {
                Write-EnvWarn -Component "kubernetes" "Warning: docker not found at /Applications/Docker.app" -ForegroundColor Yellow
            }
            Invoke-Expression "/Applications/Docker.app"
        }
        else {
            if ($path[0] -eq "/") {
                # ubuntu
                Invoke-Expression "systemctl start docker"
            }
            else {
                # windows
                if (!(Test-Path 'C:\Program Files\Docker\Docker\Docker Desktop.exe')) {
                    Write-EnvWarn -Component "kubernetes" "Warning: docker not found at C:\Program Files\Docker\Docker\Docker Desktop.exe" -ForegroundColor Yellow
                }
                Invoke-Expression "& 'C:\Program Files\Docker\Docker\Docker Desktop.exe'"
            }
        }
        # Give docker a few minutes to start
        Write-Host "Waiting 2 minutes for docker to start..."
        # Todo: Refactor to wait until docker is available
        Start-Sleep -s 120
        Write-Host "Checking docker status"
        $output = docker info | Out-String
        if ($output.Contains("error during connect")) {
            Write-EnvError -Component "kubernetes" "Could not start docker"
        } 
    }
    Write-Host "Docker seems to be running - continuing `n"

    # Set minikube home directory
    if ($minikube_home -ne "") {
        $env:MINIKUBE_HOME = $minikube_home
    }

    # Compare installed version to version set in config
    $localMinikubeVersion = $(minikube version --short) -replace '(minikube version:) (.*)', '$2'
    $setMinikubeVersion = Get-EnvMapValue -Map $config -Key "k8s.version"
    # if ($localMinikubeVersion -gt $setMinikubeVersion){
    #     Write-EnvError -Component "kubernetes" $("Locally installed minikube version ($localMinikubeVersion) is greater than the one set in the environment config ($setMinikubeVersion). " `
    #         + "Please update config's k8s.version to match the locally installed version.")
    # }

    # Start minikube
    minikube start --cpus $($cpus) `
        --memory $($memory) `
        --driver docker `
        --kubernetes-version=$($version)

    if ($LastExitCode -ne 0) {
        Write-EnvError -Component "kubernetes" "There were errors starting minikube, Watch logs above"
    }

    $k8sAddress = (minikube ip)
    $k8sSshKey = (minikube ssh-key)

    # Record results and save them to disk
    Set-EnvMapValue -Map $resources -Key "k8s.type" -Value "minikube"
    Set-EnvMapValue -Map $resources -Key "k8s.address" -Value $k8sAddress
    Set-EnvMapValue -Map $resources -Key "k8s.ssh_key" -Value $k8sSshKey
    # Record current k8s configuration to resources
    Set-EnvMapValue -Map $resources -Key "k8s.version" -Value $version
    Set-EnvMapValue -Map $resources -Key "k8s.memory" -Value $memory
    Set-EnvMapValue -Map $resources -Key "k8s.cpus" -Value $cpus
    Set-EnvMapValue -Map $resources -Key "k8s.namespace" -Value $namespace
    Set-EnvMapValue -Map $resources -Key "k8s.blobs_storage_gb" -Value $blobs_storage_gb

    Write-EnvResources -ResourcePath $ResourcePath -Resources $resources

    # Wait for minikube to start
    # Todo: rewrite to use "minikube status"
    do {
        Write-Host "Waiting for minikube to start..."
        Start-Sleep -Seconds 5
        $out = kubectl get nodes | Out-String
    } while (!($out.Contains("Ready")));

    Write-EnvInfo -Component "kubernetes" "Started creating k8s namespace and blobs persistent volume." -Delimiter

    $templateParams = @{
        namespace        = $namespace;
        blobs_storage_gb = $blobs_storage_gb
    }

    # Set variables from config
    Build-EnvTemplate -InputPath "$PSScriptRoot/templates/namespace.yml" -OutputPath "$PSScriptRoot/../temp/namespace.yml" -Params1 $templateParams

    # Create k8s namespace
    kubectl apply -f "$PSScriptRoot/../temp/namespace.yml"

    # Enable ingress controller
    minikube addons enable ingress

    # Set variables from config
    Build-EnvTemplate -InputPath "$PSScriptRoot/templates/blobs_pv.yml" -OutputPath "$PSScriptRoot/../temp/blobs_pv.yml" -Params1 $templateParams

    # Create k8s blobs persistent volume
    kubectl apply -f "$PSScriptRoot/../temp/blobs_pv.yml"
}
else {
    return $false
}

Switch-EnvKubeContext
