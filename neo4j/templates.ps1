#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [hashtable] $Config,

    [Parameter(Mandatory = $true, Position = 1)]
    [hashtable] $Resources
)

# Read variables
$namespace = Get-EnvMapValue -Map $config -Key "k8s.namespace"
$version = Get-EnvMapValue -Map $config -Key "neo4j.version"
$name = Get-EnvMapValue -Map $config -Key "neo4j.name"
$username = Get-EnvMapValue -Map $config -Key "neo4j.username"
$password = Get-EnvMapValue -Map $config -Key "neo4j.password"

# Set template parameters
$templateParams = @{
    namespace = $namespace;
    version = $version;
    name = $name;
    username = $username;
    password = $password 
}

# Generate template
Build-EnvTemplate -InputPath "$PSScriptRoot/templates/neo4j.yml" -OutputPath "$PSScriptRoot/../temp/neo4j.yml" -Params1 $templateParams
