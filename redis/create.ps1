#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [string] $ConfigPath,

    [Parameter(Mandatory = $true, Position = 1)]
    [string] $ResourcePath
)

# Read config and resources
$config = Read-EnvConfig -ConfigPath $ConfigPath
$resources = Read-EnvResources -ResourcePath $ResourcePath

# Generate template
. "$PSScriptRoot/templates.ps1" -Config $config -Resources $resources

# Create kubernetes component
kubectl apply -f "$PSScriptRoot/../temp/redis.yml"

# Record results and save them to disk
$port = kubectl get svc redis -n $namespace -o=jsonpath="{.spec.ports[0].targetPort}"
$serviceHost = "redis." + $namespace + ".svc.cluster.local"
$endpoint = "$($serviceHost):$($port)"

Set-EnvMapValue -Map $resources -Key "redis.port" -Value $port
Set-EnvMapValue -Map $resources -Key "redis.host" -Value $serviceHost
Set-EnvMapValue -Map $resources -Key "redis.endpoint" -Value $endpoint
Write-EnvResources -ResourcePath $ResourcePath -Resources $resources
