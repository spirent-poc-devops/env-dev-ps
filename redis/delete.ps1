#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [string] $ConfigPath,

    [Parameter(Mandatory = $true, Position = 1)]
    [string] $ResourcePath
)

# Read config and resources
$config = Read-EnvConfig -ConfigPath $ConfigPath
$resources = Read-EnvResources -ResourcePath $ResourcePath

# Generate template
. "$PSScriptRoot/templates.ps1" -Config $config -Resources $resources

kubectl delete --ignore-not-found=true -f "$PSScriptRoot/../temp/redis.yml"

if ($LastExitCode -ne 0) {
    Write-EnvError -Component "redis" "There were errors deleting redis, Watch logs above"
}

# Delete results and save resource file to disk
if (Test-EnvMapValue -Map $resources -Key "redis") {
    Remove-EnvMapValue -Map $resources -Key "redis.port"
    Remove-EnvMapValue -Map $resources -Key "redis.host"
    Remove-EnvMapValue -Map $resources -Key "redis.endpoint"
    Write-EnvResources -ResourcePath $ResourcePath -Resources $resources
}
