#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [hashtable] $Config,

    [Parameter(Mandatory = $false, Position = 1)]
    [hashtable] $Resources
)

# Read parameters
$namespace = Get-EnvMapValue -Map $config -Key "k8s.namespace"
$replicas_count = Get-EnvMapValue -Map $config -Key "redis.replicas_count"
$password = Get-EnvMapValue -Map $config -Key "redis.password"

# Set template parameters
$templateParams = @{
    namespace = $namespace;
    replicas_count = $replicas_count;
    password = $password 
}

# Generate template
Build-EnvTemplate -InputPath "$PSScriptRoot/templates/redis.yml" -OutputPath "$PSScriptRoot/../temp/redis.yml" -Params1 $templateParams
