#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [hashtable] $Config,

    [Parameter(Mandatory = $false, Position = 1)]
    [hashtable] $Resources
)

# Read parameters
$namespace = Get-EnvMapValue -Map $config -Key "k8s.namespace"
$version = Get-EnvMapValue -Map $config -Key "timescale.version"
$name = Get-EnvMapValue -Map $config -Key "timescale.name"
$username = Get-EnvMapValue -Map $config -Key "timescale.username"
$password = Get-EnvMapValue -Map $config -Key "timescale.password"

# Set template parameters
$templateParams = @{
    namespace = $namespace;
    version   = $version;
    name      = $name;
    username  = $username;
    password  = $password
}

# Generate template
Build-EnvTemplate -InputPath "$PSScriptRoot/templates/timescale.yml" -OutputPath "$PSScriptRoot/../temp/timescale.yml" -Params1 $templateParams
