#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [string] $ConfigPath,

    [Parameter(Mandatory = $true, Position = 1)]
    [string] $ResourcePath
)

# Read config and resources
$config = Read-EnvConfig -ConfigPath $ConfigPath
$resources = Read-EnvResources -ResourcePath $ResourcePath

# Generate template
. "$PSScriptRoot/templates.ps1" -Config $config -Resources $resources

# Create kubernetes component
kubectl apply -f "$PSScriptRoot/../temp/timescale.yml"

# Record results and save them to disk
$port = kubectl get svc timescale -n $namespace -o=jsonpath="{.spec.ports[0].targetPort}"
$serviceHost = "timescale." + $namespace + ".svc.cluster.local"
$endpoint = "$($serviceHost):$($port)"

Set-EnvMapValue -Map $resources -Key "timescale.port" -Value $port
Set-EnvMapValue -Map $resources -Key "timescale.host" -Value $serviceHost
Set-EnvMapValue -Map $resources -Key "timescale.endpoint" -Value $endpoint
Write-EnvResources -ResourcePath $ResourcePath -Resources $resources
