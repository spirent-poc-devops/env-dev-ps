#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [hashtable] $Config,

    [Parameter(Mandatory = $true, Position = 2)]
    [hashtable] $Resources
)

# Read parameters
$namespace = Get-EnvMapValue -Map $config -Key "k8s.namespace"

# Set template parametrs
$templateParams = @{
    namespace = $namespace;
}

# Generate template
Build-EnvTemplate -InputPath "$PSScriptRoot/templates/logging.yml" -OutputPath "$PSScriptRoot/../temp/logging.yml" -Params1 $templateParams
