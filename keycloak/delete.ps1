#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [string] $ConfigPath,

    [Parameter(Mandatory = $true, Position = 1)]
    [string] $ResourcePath
)

# Read config and resources
$config = Read-EnvConfig -ConfigPath $ConfigPath
$resources = Read-EnvResources -ResourcePath $ResourcePath

$namespace = Get-EnvMapValue -Map $config -Key "k8s.namespace"

# Delete component
helm uninstall keycloak -n $namespace

# Delete results and save resource file to disk
if (Test-EnvMapValue -Map $resources -Key "keycloak") {
    Remove-EnvMapValue -Map $resources -Key "keycloak.state"
    Remove-EnvMapValue -Map $resources -Key "keycloak.port"
    Remove-EnvMapValue -Map $resources -Key "keycloak.host"
    Remove-EnvMapValue -Map $resources -Key "keycloak.endpoint"
    Write-EnvResources -ResourcePath $ResourcePath -Resources $resources
}
