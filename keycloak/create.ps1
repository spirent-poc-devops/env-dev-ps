#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [string] $ConfigPath,

    [Parameter(Mandatory = $true, Position = 1)]
    [string] $ResourcePath
)

# Read config and resources
$config = Read-EnvConfig -ConfigPath $ConfigPath
$resources = Read-EnvResources -ResourcePath $ResourcePath

$namespace = Get-EnvMapValue -Map $config -Key "k8s.namespace"

$templateParams = @{
  namespace = $namespace
  keycloak_username = Get-EnvMapValue -Map $config -Key "keycloak.username"
  keycloak_password = Get-EnvMapValue -Map $config -Key "keycloak.password"
  db_host = Get-EnvMapValue -Map $resources -Key "timescale.host"
  db_port = Get-EnvMapValue -Map $resources -Key "timescale.port"
  db_name = Get-EnvMapValue -Map $config -Key "timescale.name"
  db_username = Get-EnvMapValue -Map $config -Key "timescale.username"
  db_password = Get-EnvMapValue -Map $config -Key "timescale.password"
}

Build-EnvTemplate -InputPath "$PSScriptRoot/templates/values.yml" -OutputPath "$PSScriptRoot/../temp/keycloak_values.yml" -Params1 $templateParams

# Todo: replace helm chart with a yml file to use private images
helm repo add codecentric https://codecentric.github.io/helm-charts
helm install keycloak codecentric/keycloak `
  --values "$PSScriptRoot/../temp/keycloak_values.yml" `
  --namespace $namespace

# Enable apisix plugin
$k8sHostIp = Get-EnvMapValue -Map $resources -Key "k8s.private_ip"
$keycloakPort = $(kubectl get --namespace $namespace -o jsonpath="{.spec.ports[0].nodePort}" services keycloak-http)
$apisixGWPort = Get-EnvMapValue -Map $resources -Key "apisix.port"
$apisixAdminPort = Get-EnvMapValue -Map $resources -Key "apisix.admin_port"
$apisixAdminToken = Get-EnvMapValue -Map $resources -Key "apisix.admin_token"
$Body = @"
{
    "uri": "/get",
    "plugins": {
        "authz-keycloak": {
            "token_endpoint": "http://$k8sHostIp`:$keycloakPort/auth/realms/master/protocol/openid-connect/token",
            "permissions": ["hello#view"],
            "audience": "account"
        }
    },
    "upstream": {
        "type": "roundrobin",
        "nodes": {
            "$k8sHostIp`:$apisixGWPort": 1
        }
    }
}
"@
Write-Host "Enabling apisix keycloak plugin..."
$null = Invoke-WebRequest "$k8sHostIp`:$apisixAdminPort/apisix/admin/routes/1" -Headers @{'X-API-KEY'=$apisixAdminToken} -Body $Body -Method 'PUT'

# Record results and save them to disk
$serviceHost = "keycloak-http." + $namespace + ".svc.cluster.local"
$endpoint = "$($serviceHost):$($keycloakPort)"

Set-EnvMapValue -Map $resources -Key "keycloak.state" -Value "deployed"
Set-EnvMapValue -Map $resources -Key "keycloak.port" -Value $keycloakPort
Set-EnvMapValue -Map $resources -Key "keycloak.host" -Value $serviceHost
Set-EnvMapValue -Map $resources -Key "keycloak.endpoint" -Value $endpoint
Write-EnvResources -ResourcePath $ResourcePath -Resources $resources
